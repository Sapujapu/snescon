#!/usr/bin/zsh

#options
srcFile=main.cpp
buildDir=./build
esptool_bin="/home/fabian/.arduino15/packages/esp8266/tools/esptool/0.4.9/esptool"
port=/dev/ttyUSB0
baud=921600
#baud=600

mkdir $buildDir
#build
#/usr/share/arduino/arduino-builder -dump-prefs -logger=machine -hardware /usr/share/arduino/hardware -hardware /home/fabian/.arduino15/packages -tools /usr/share/arduino/tools-builder -tools /home/fabian/.arduino15/packages -built-in-libraries /usr/share/arduino/libraries -libraries /home/fabian/Arduino/libraries -fqbn=esp8266:esp8266:nodemcuv2:CpuFrequency=80,UploadSpeed=921600,FlashSize=4M3M -ide-version=10800 -build-path /home/fabian/Programmieren/ESP/Blink/build -warnings=none -prefs=build.warn_data_percentage=75 -prefs=runtime.tools.esptool.path=/home/fabian/.arduino15/packages/esp8266/tools/esptool/0.4.9 -prefs=runtime.tools.mkspiffs.path=/home/fabian/.arduino15/packages/esp8266/tools/mkspiffs/0.1.2 -prefs=runtime.tools.xtensa-lx106-elf-gcc.path=/home/fabian/.arduino15/packages/esp8266/tools/xtensa-lx106-elf-gcc/1.20.0-26-gb404fb9-2 -verbose $srcFile
/usr/share/arduino/arduino-builder -compile -logger=machine -hardware /usr/share/arduino/hardware -hardware /home/fabian/.arduino15/packages -tools /usr/share/arduino/tools-builder -tools /home/fabian/.arduino15/packages -built-in-libraries /usr/share/arduino/lib -libraries /home/fabian/Arduino/libraries -fqbn=esp8266:esp8266:nodemcuv2:CpuFrequency=80,UploadSpeed=921600,FlashSize=4M3M -ide-version=10800 -build-path $(pwd)/$buildDir -warnings=none -prefs=build.warn_data_percentage=75 -prefs=runtime.tools.esptool.path=/home/fabian/.arduino15/packages/esp8266/tools/esptool/0.4.9 -prefs=runtime.tools.mkspiffs.path=/home/fabian/.arduino15/packages/esp8266/tools/mkspiffs/0.1.2 -prefs=runtime.tools.xtensa-lx106-elf-gcc.path=/home/fabian/.arduino15/packages/esp8266/tools/xtensa-lx106-elf-gcc/1.20.0-26-gb404fb9-2 -verbose $srcFile&&

#flash
$esptool_bin -vv -cd nodemcu -cb $baud -cp $port -ca 0x00000 -cf $buildDir/$srcFile.bin &&

#connect to serial
#tio $port
